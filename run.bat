@echo off
title Java Fractal Generator V6.0.0
set epsilon=" "
set maxIterations=" "
set maxColors=" "
set /p epsilon="epsilon: "
set /p maxIterations="maxIterations: "
set /p maxColors="maxColors: "
cls
java -jar .\dist\Fractal_Generator.jar %epsilon% %maxIterations% %maxColors%
pause