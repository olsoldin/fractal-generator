package fractal;

import java.awt.image.BufferedImage;

/**
 *
 * @author Oliver
 */
public class Settings {

	protected static final int BOUND = 3;		//the min and max values for the x and y planes (eg. bnound 2 will calculate between -2 and 2)
	protected static final int NUM_THREADS = (BOUND * 2) * (BOUND * 2); // X & Y from -BOUND to +BOUND 
	protected static int threadImageSize;
	protected static BufferedImage[] image = new BufferedImage[NUM_THREADS];

	protected double epsilon = 0.002;			// The step size across the X and Y axis (default: 0.002)
	protected int maxIterations = 100;			// increasing this will give you a more detailed fractal (default: 1000)
	protected double maxColours = 255;			// Change as appropriate for your display. (default: 255)
	protected float startTime, finishTime;		//Used for displaying how long the calculations took
	protected double epsilon_D = 1 / epsilon;		//used a lot in calculating the x and y coordinates

	protected void setEpsilon(double e) {
		epsilon = e;
		epsilon_D = 1 / epsilon;
	}
}
