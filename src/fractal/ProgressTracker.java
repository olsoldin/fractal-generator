/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fractal;

/**
 *
 * @author Oliver
 */
public class ProgressTracker {

	private static long numCalcs = 0;
	public static float startTime, endTime;	//Used for displaying how long the calculations took
	private static int percent;
	protected static int onePercent = 0;

	public static void start() {
		startTime = System.nanoTime();
	}

	public static void stop() {
		endTime = System.nanoTime();
	}

	public static void increment() {
		numCalcs++;
	}

	/**
	 * due to rounding errors with all the casting to (int)s in the program, it
	 * can occasionally finish at 101% or 99% or some other variant so just
	 * replace this with 100% so the user doesn't think it hasn't finished
	 * calculating (this code is only run once the image is ready to be written
	 * anyway)
	 */
	public static void printCalculations() {
		System.out.println("\n" + Utils.number(ProgressTracker.numCalcs) + " calculations complete in "
				+ Utils.displaySeconds((ProgressTracker.endTime - ProgressTracker.startTime) / 1_000_000_000));
		System.out.println("\nProcessing image...");
	}

	public static void printProgress() {
		int modifier = (Settings.BOUND * 2);
		System.out.print(percent++ / modifier + "%\r");//prints the percentage then increments

		if (percent > 100 * modifier) {
			percent = 100 * modifier;
		}
	}
}
