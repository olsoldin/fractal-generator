/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fractal;

/**
 *
 * @author Oliver
 */
public class Utils {

	/**
	 * Takes a number of seconds and returns a string in the form
	 * "hrs:mins:seconds"
	 *
	 * @param seconds the number of seconds to convert
	 *
	 * @return the same time as hrs:mins:secs
	 */
	public static String displaySeconds(float seconds) {
		int hours = (int) (seconds / 3600);
		int mins = (int) ((seconds % 3600) / 60);
		seconds %= 60;

		return (asTwoDigits(hours) + ":" + asTwoDigits(mins) + ":" + (seconds)
				+ "s");
	}

	/**
	 * Takes a float between 0 and 99 as an input and returns it with 2 digits
	 *
	 * @param d the float to be formatted
	 *
	 * @return the formatted number as a String
	 */
	public static String asTwoDigits(float d) {
		return d < 10 ? "0" + (int) d : "" + (int) d;
	}

	/**
	 * takes any number as an input and formats it with a comma between every 3
	 * 0's
	 *
	 * @param obj number to be formatted
	 *
	 * @return the formatted number as a String
	 */
	public static final String number(Object obj) {
		String ans = "" + ((Number) obj).longValue();
		String num = new StringBuffer(ans).reverse().toString();
		ans = "";

		for (int i = num.length(); i > 0; i--) {
			ans += (((i % 3 == 0 && i < num.length()) ? "," : "") + num.charAt(i - 1));
		}

		return ans;
	}

	/**
	 * Converts a double d between min and max, to an int between 0 and
	 * outputMax
	 *
	 * eg. if d == max then return outputMax
	 * if d == min then return 0
	 *
	 * @param d double value to map to a value between 0 and outputMax
	 * @param min the min value parameter d will ever be
	 * @param max the max value parameter d will ever be
	 * @param outputMax the maximum value the output will ever be
	 * @return d mapped to a value between 0 and outputMax
	 */
	public static int map(double d, double min, double max, int outputMax) {
		double rangeSize = max - min;
		double mappedTo0to1 = (d - min) / rangeSize; // Map range 0 - 1
		return (int) (mappedTo0to1 * outputMax);
	}
}
