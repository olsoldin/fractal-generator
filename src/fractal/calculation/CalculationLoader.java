package fractal.calculation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;

public class CalculationLoader extends ClassLoader {
	
	private File calculationDir;
	private String m_root;
	
	public CalculationLoader(String root) throws FileNotFoundException {
		this(CalculationLoader.class.getClassLoader(), root);
		this.calculationDir = new File(m_root);
	}
	
	public CalculationLoader(ClassLoader parent, String root) throws FileNotFoundException {
		super(parent);
		
		File f = new File(root);
		
		if(f.isDirectory()){
			m_root = root;
		}else{
			throw new FileNotFoundException();
		}
	}
	
	public byte[] findClassBytes(String className){
		try {
			String pathName = m_root + File.separatorChar + className;
			
			FileInputStream inFile = new FileInputStream(pathName);
			byte[] classBytes = new byte[inFile.available()];
			inFile.read(classBytes);
			
			return classBytes;
		}catch(IOException e){
			return null;
		}
	}
	
	@Override
	public Class findClass(String name) throws ClassNotFoundException{
		byte[] classBytes = findClassBytes(name);
		if(classBytes == null){
			throw new ClassNotFoundException();
		}else{
			return super.defineClass(name.substring(0, name.length() - 6), classBytes, 0, classBytes.length);
		}
	}
	
	public AbstractCalculation loadCalculation(String fileName){
		Class newCalcClass;
		AbstractCalculation newCalc = null;
		
		try{
			newCalcClass = this.findClass(fileName);
			if(this.validateClass(newCalcClass) == true){
				newCalc = (AbstractCalculation)newCalcClass.newInstance();
			}
		}catch(ClassNotFoundException | InstantiationException | IllegalAccessException e){
			System.err.println(e);
			System.exit(2);
		}
		return newCalc;
	}
	
	public String[] findCalculations(){
		return this.calculationDir.list(new CalcFileFilter());
	}
	
	private boolean validateClass(Class calcClass){
		String className;
		
		if(calcClass == null){
			return false;
		}
		
		className = calcClass.getSuperclass().getName();
		
		if(className.compareTo("AbstractCalculation") != 0){
			return false;
		}
		
		return true;
	}
	
	public class CalcFileFilter implements FilenameFilter {
		@Override
		public boolean accept(File fileDirectory, String fileName){
			return fileName.startsWith("calc") && fileName.contains(".class");
		}
	}
}
