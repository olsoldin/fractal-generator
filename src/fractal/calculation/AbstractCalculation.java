package fractal.calculation;

import org.apache.commons.math3.complex.Complex;

/**
 *
 * @author Oliver
 */
public abstract class AbstractCalculation {

	public abstract Complex calc(Complex Z, Complex C);
	
	public abstract String getName();
}
