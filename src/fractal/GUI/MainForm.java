/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractal.GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *  WIP
 * @author Oliver
 */
public class MainForm extends JFrame{

    private static JPanel panel = new JPanel();
    //--//
    private Graphics g;

    public static void main(String[] args){
        new MainForm("Fractal");
    }

    public MainForm(){
        this("MainForm");
    }

    public MainForm(String title){
        this(200, 200, title);
    }

    public MainForm(int width, int height){
        this(width, height, "");
    }

    public MainForm(int width, int height, String title){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle(title);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setPreferredSize(new Dimension(width, height));
        this.add(panel);
        this.pack();
        this.setVisible(true);
        g = getGraphics();
    }

    public void setPanelColour(Color colour){
        panel.setBackground(colour);
    }

    /**
     * Draws the pixel to the panel
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param colour colour of the pixel
     */
    public void drawPixel(int x, int y, Color colour){
        bufferPixel(x, y, colour);
        paintBuffer();
    }

    /**
     * same as drawPixel(int x, int y, Color colour) only with rgb
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param r red value (0 - 255)
     * @param g green value (0 - 255)
     * @param b blue value (0 - 255)
     */
    public void drawPixel(int x, int y, int r, int g, int b){
        drawPixel(x, y, new Color(r, g, b));
    }

    /**
     * Buffers the pixel without drawing it, call paintBuffer() to draw to the panel
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param colour colour of the pixel
     */
    public void bufferPixel(int x, int y, Color colour){
        g.setColor(colour);
        g.drawLine(x, y, x, y);
    }

    /**
     * Same as bufferPixel(int x, int y, Color colour) only with rgb
     *
     * @param x x-coordinate
     * @param y y-coordinate
     * @param r red value (0 - 255)
     * @param g green value (0 - 255)
     * @param b blue value (0 - 255)
     */
    public void bufferPixel(int x, int y, int r, int g, int b){
        bufferPixel(x, y, new Color(r, g, b));
    }

    /**
     * Paints the buffer to the panel
     */
    public void paintBuffer(){
        panel.paintComponents(g);
        this.pack();
    }

    /**
     * Gets the panel
     *
     * @return panel in use in the frame
     */
    public JPanel getPanel(){
        return panel;
    }
}
