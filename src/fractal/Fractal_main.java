package fractal;

/**
 * Generates a fractal and saves it to the users hard drive with thanks to stack
 * overflow user abelenky for the main algorithm for the generation
 * http://stackoverflow.com/questions/425953/how-to-program-a-fractal
 *
 * Java Fractal Generator
 * @version V6.2.0
 * @author Oliver Olding
 */
public class Fractal_main {

	private Settings settings;
	private ImageHandler imageHandler;

	public static void main(String[] args) {
		/*
		 * adds a hook to the shutdown event
		 * whenever the program is terminated (either by user, a crash, or
		 * by completion of the algorithm) it starts a new thread to output
		 * "Shutting down..." to the terminal
		 */
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			System.out.println("\nShutting Down");
		}));

		Settings s = new Settings();

		/*
		 * takes the arguments in order and attempts to assign them to the
		 * correct settings, if this fails it shows a usage message to
		 * the user and continues the execution with the default settings
		 */
		try {
			if (args.length >= 1) {
				s.setEpsilon(Float.parseFloat(args[0]));
			}
			if (args.length >= 2) {
				s.maxIterations = Integer.parseInt(args[1]);
			}
			if (args.length >= 3) {
				s.maxColours = Float.parseFloat(args[2]);
			}
		} catch (NumberFormatException e) {
			System.out.println("Usage: Fractal_Visualiser.jar (float)Epsilon "
					+ "(integer)MaxIterations (float)MaxColours\nContinuing with default settings.\n");
		}

		System.out.println("epsilon:       " + s.epsilon);
		System.out.println("maxIterations: " + s.maxIterations);
		System.out.println("maxColors:     " + (int) s.maxColours);
		System.out.println("Running...");

		//no need to assign the object because its only run once
		new Fractal_main(s);
	}

	/**
	 * Generates a fractal and saves the image to a PNG file on the hard drive.
	 * the mandlebrot set is built in as a method, all other fractals can be
	 * easily added to the generation code by adding another method and calling
	 * it in the appropriate place
	 *
	 * @param s default settings
	 */
	public Fractal_main(Settings s) {
		settings = s;
		// One image generated per thread
		imageHandler = new ImageHandler(settings.NUM_THREADS);

		/*
		 * the width / height in pixels of the
		 * generated image, the two loops repeat
		 * from -2 to 2 with a step of 1/epsilon
		 */
		int threadImageSize = (int) settings.epsilon_D;
		settings.threadImageSize = threadImageSize;

		/*
		 * The percentage shows how far along the x coordinate it's done
		 * in general the percentage is accurate and the y coordinate
		 * is not needed.
		 * There are sqrt(NUM_THREADS) individual images in the X direction
		 * The final image is an exact square, if there are 4 threads it is 2x2
		 * each image has the size threadImageSize
		 * onePercent is the progress of a single percent across the X value
		 */
		ProgressTracker.onePercent = (int) (threadImageSize * Math.sqrt(settings.NUM_THREADS)) / 100;

		ProgressTracker.start();

		/*
		 * Creates an array of threads to do the calculations.
		 * Each thread calcualtes a small portion of the image
		 */
		Thread[] thread = new Thread[settings.NUM_THREADS];
		//starts all threads computing the fractal
		int x = -settings.BOUND;
		int y = -settings.BOUND;
		for (int i = 0; i < settings.NUM_THREADS; i++) {
			thread[i] = new Thread(new Fractal(settings, x, x + 1, y, y + 1));
			x++;
			thread[i].start();
			if (x == settings.BOUND) {
				x = -settings.BOUND;
				y++;
			}
		}
		//waits for all threads to complete
		try {
			for (int i = 0; i < settings.NUM_THREADS; i++) {
				thread[i].join();
			}
		} catch (InterruptedException e) {
			System.err.print("Thread was interrupted unexpectedly");
		}

		ProgressTracker.stop();

		ProgressTracker.printCalculations();

		/* 
		 * Create image from NUM_THREADS individual images (each thread creates its own small part of the image)
		 * The size of each part is threadImageSize
		 */
		imageHandler.createFinalImage(settings.NUM_THREADS, threadImageSize, "Fractal.png");

		System.out.println("\rComplete");
	}
}
