package fractal;

import java.awt.image.BufferedImage;
import java.util.Random;
import org.apache.commons.math3.complex.Complex;

/**
 *
 * @author Oliver
 */
public class Fractal implements Runnable {

	private static int index = 0;
	private final int xlowerBound, xupperBound, ylowerBound, yupperBound;
	private final int storedIndex;
	//gets a random number between 0 and 16,777,215, for debuggins
	private final int rnd = new Random(System.nanoTime() * 1000).nextInt(0xFFFFFF);

	private final Settings settings;

	public Fractal(Settings s, int xlowerBound, int xupperBound, int ylowerBound, int yupperBound) {
		settings = s;

		this.xlowerBound = xlowerBound;
		this.xupperBound = xupperBound;
		this.ylowerBound = ylowerBound;
		this.yupperBound = yupperBound;
		this.storedIndex = index++;
		ImageHandler.image[storedIndex] = new BufferedImage(Settings.threadImageSize, Settings.threadImageSize, BufferedImage.TYPE_4BYTE_ABGR_PRE);
	}

	//<editor-fold defaultstate="collapsed" desc="Fractal Equations">
	/**
	 * Does the calculation for a mandlebrot fractal (Z1 = Z0^2 + C)
	 *
	 * @param Z Complex Z
	 * @param C Complex C
	 *
	 * @return Z^2 + C
	 */
	private Complex mandlebrot(Complex Z, Complex C) {
		return Z.multiply(Z).add(C);
	}

	/**
	 * The user can edit this one to their hearts content
	 *
	 * @param Z Complex Z
	 * @param C Complex C
	 *
	 * @return Whatever the user wants
	 */
	private Complex julia(Complex Z, Complex C) {
		C = C.divide(3).multiply(C);
		return Z.multiply(Z).add(C);
	}

	/**
	 * The mandlebrot fractal but using the equation Z1 = Z0^3 + C
	 *
	 * @param Z Complex Z
	 * @param C Complex C
	 *
	 * @return Z^3 + C
	 */
	private Complex mandlebrotCubed(Complex Z, Complex C) {
		return Z.multiply(Z.multiply(Z)).add(C);
	}
	//</editor-fold>

	/**
	 * Maps a given Hue value (0 - maxColors) or (0 - 255) as default to an RGB
	 * value ^ 0xFF000000 is to make the alpha layer opaque
	 *
	 * @param d the hue to convert
	 *
	 * @return RGB value in the range 0x000000 - 0xFFFFFF
	 */
	private int intToRGB(double d) {
		//return (int)(d * 0xD15EA5E) | 0xFF000000; //(bright green)
//        return (int)(d * rnd) | 0xFF000000; //(patches representing each thread)
//        return (int)(d * 0xB00B5) | 0xFF000000; //(Orange - Purple - Blue  - Black)
//        return (int)(d * 0xB00B) | 0xFF000000; //(Purple - Black - Green)
		//return (int)d | 0xFF000000;
		return (int) (d * 0xD1C1D3D) | 0xFF000000;
	}

	private int adjustToInt(double d) {
		return (int) (d * settings.epsilon_D + 2 * settings.epsilon_D);
	}

	@Override
	public void run() {
		double iterations;
		for (float x = xlowerBound; x <= xupperBound; x += settings.epsilon) {
			for (float y = ylowerBound; y <= yupperBound; y += settings.epsilon) {
				iterations = 0;
				/*
                 * create a new constant complex number representing the
                 * coordinates of the current pixel
				 */
				Complex C = new Complex(x, y);
				//the starting value of z
				Complex Z = new Complex(0, 0);
				/*
                 * while z is within the bounds of the image and it isnt stuck
                 * in a loop (reaches maxIterations)
				 */
				while (Z.abs() < settings.BOUND && iterations < settings.maxIterations) {
					//iterates the calculations for the image
//                    Z = mandlebrot(Z, C);
					Z = julia(Z, C);
					// to generate
					iterations++;
					ProgressTracker.increment();
				}

				/*
                 * set the pixel at (x, y) to a colour set by the number
                 * of iterations it took for Z to either reach "infinity" or
                 * if its stuck in a loop
				 */
				int xPos = Utils.map(x, xlowerBound, xupperBound, settings.threadImageSize);
				int yPos = Utils.map(y, ylowerBound, yupperBound, settings.threadImageSize);
				try {
					ImageHandler.image[storedIndex].setRGB(xPos, yPos, intToRGB(settings.maxColours % iterations));
				} catch (ArrayIndexOutOfBoundsException e) {
					System.err.println("Error at: (" + xPos + ", " + yPos + ") \"Index out of bounds\"");
				}

			}

			//should only be true 100 times through any calculations
			if (adjustToInt(x) % ProgressTracker.onePercent == 0) {
				ProgressTracker.printProgress();
			}
		}
	}
}
