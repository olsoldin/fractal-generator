package fractal;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Oliver
 */
public class ImageHandler {

	protected static BufferedImage[] image;

	public ImageHandler(int numImages) {
		image = new BufferedImage[numImages];
	}

	/**
	 * Creates a new file and attempts to write the buffered image to it by
	 * converting the raw data to a compressed PNG file PNG was used as JPG
	 * loses too much data, and a raw BMP file could reach in excess of 1GB
	 * where the same image with the same detail can be stored at around 8MB as
	 * a PNG file
	 * 
	 * Everything is assuming a perfectly square image (2x2, 3x3, 10x10 etc.)
	 *
	 * @param numSections	Number of individual images that make up the final
	 * image
	 * @param section	Width and Height of each individual section
	 * @param fileName The output image name, including the file extension (eg.
	 * "Fractal.png")
	 */
	public void createFinalImage(int numSections, int section, String fileName) {
		// number of individual images in each column and row
		// EG. numsections = 16
		// 4 x 4 grid of images
		int xyNum = (int) Math.sqrt(numSections);

		// Total width + height of the final image
		// equal to the size of each section x the number of sections in a row / column
		int totalSize = section * xyNum;

		File oFile = new File(fileName);
		try {
			BufferedImage finalImage = new BufferedImage(totalSize, totalSize, BufferedImage.TYPE_4BYTE_ABGR_PRE);

			Graphics g = finalImage.getGraphics();

			int x = 0;
			int y = 0;
			for (int i = 0; i < numSections; i++) {
				g.drawImage(image[i], x, y, null);
				x += section;
				if (x == totalSize) {
					x = 0;
					y += section;
				}
			}
			ImageIO.write(finalImage, "PNG", oFile);

		} catch (IOException e) {
			System.out.println("Error: file " + oFile.getName() + " is not accesable.");
		}
	}

}
